# OpenML dataset: house_sales

https://www.openml.org/d/44019

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Dataset used in the tabular data benchmark https://github.com/LeoGrin/tabular-benchmark, transformed in the same way. This dataset belongs to the "regression on numerical features" benchmark. Original description: 
 
Date converted to year/mo/day numerics.This dataset contains house sale prices for King County, which includes Seattle. It includes homes sold between May 2014 and May 2015.

It contains 19 house features plus the price and the id columns, along with 21613 observations.
It's a great dataset for evaluating simple regression models.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44019) of an [OpenML dataset](https://www.openml.org/d/44019). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44019/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44019/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44019/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

